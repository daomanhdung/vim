"@Plugins
call plug#begin('~/.vim/plugged')

"@Language-Plugins
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
Plug 'herringtondarkholme/yats.vim'

"@Workspace-Plugins
Plug 'preservim/nerdtree'
Plug 'frazrepo/vim-rainbow'
Plug 'tpope/vim-commentary'
Plug 'morhetz/gruvbox'
Plug 'itchyny/lightline.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

call plug#end()

"Setting
set background=dark
colorscheme gruvbox
let g:lightline = {'colorscheme': 'gruvbox', }

syntax on
filetype on 
filetype plugin on
filetype indent on

set hidden
set wrap
set mouse=a  
set hlsearch 
set incsearch 
set encoding=UTF-8
set nobackup
set cmdheight=1
set nowritebackup
set termguicolors
set clipboard+=unnamedplus
set number relativenumber
let mapleader = "\<Space>" 

"@Keybinding
nnoremap <silent> <Tab> <C-w>w
nnoremap <nowait> <C-q> :q<CR>
map <silent> <C-n> :NERDTreeToggle<CR>
map <silent> ,, <ESC>
map <Enter> o<ESC>
:imap <silent> ,, <Esc>

"FZF-Keybinding
nnoremap <silent> <leader>f :FZF<CR>

set wildmode=list:longest,list:full
set wildignore+=*.o,*.obj,.git,*.rbc,*.pyc,__pycache__
let $FZF_DEFAULT_COMMAND =  "find * -path '*/\.*' -prune -o -path '**/node_modules/**' -prune -o -path 'node_modules/**' -prune -o -path 'target/**' -prune -o -path 'dist/**' -prune -o  -type f -print -o -type l -print 2> /dev/null"